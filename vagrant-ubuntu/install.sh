#!/bin/bash 

# 修正中文字体年月显示问题
# block1="zh_CN UTF-8 UTF-8
# zh_CN GB2312
# en_US.UTF-8 UTF-8
# "
# echo "$block1" | sudo tee "/var/lib/locales/supported.d/local"

# sudo locale-gen

# block2="LANG=\"zh_CN.UTF-8\"
# LANGUAGE=\"zh_CN:zh\"
# LC_NUMERIC=\"zh_CN.UTF-8\"
# LC_TIME=\"zh_CN.UTF-8\"
# LC_MONETARY=\"zh_CN.UTF-8\"
# LC_PAPER=\"zh_CN.UTF-8\"
# LC_NAME=\"zh_CN.UTF-8\"
# LC_ADDRESS=\"zh_CN.UTF-8\"
# LC_TELEPHONE=\"zh_CN.UTF-8\"
# LC_MEASUREMENT=\"zh_CN.UTF-8\"
# LC_IDENTIFICATION=\"zh_CN.UTF-8\"
# "
# echo "$block2" | sudo tee "/etc/default/locale"

# 添加nginx软件源
nginx_list="/etc/apt/sources.list.d/nginx.list"

if [ ! -f "$nginx_list" ]; then
  wget -O - http://nginx.org/keys/nginx_signing.key | sudo apt-key add -
  echo 'deb http://nginx.org/packages/ubuntu/ trusty nginx' | sudo tee /etc/apt/sources.list.d/nginx.list
  echo 'deb-src http://nginx.org/packages/ubuntu/ trusty nginx' | sudo tee -a /etc/apt/sources.list.d/nginx.list
fi

# 添加php源
sudo add-apt-repository ppa:ondrej/php

# 更新源
sudo apt-get update

# 安装nginx
sudo apt-get install nginx -y
# 安装php源
sudo apt-get install php7.0-cli php7.0-fpm php7.0-gd php7.0-curl php7.0-mcrypt php7.0-mysql php7.0-mbstring php7.0-xml -y

# 安装mysql
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'
sudo apt-get install mysql-server -y

# 安装系统管理
sudo apt-get install sysv-rc-conf -y
sudo sysv-rc-conf nginx on

sudo service nginx restart

exit