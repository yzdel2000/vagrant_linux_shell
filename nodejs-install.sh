#!/bin/bash
# 需要指定版本比如 ./nodejs-install.sh 1.8.1

wget https://nodejs.org/dist/v$1/node-v$1-linux-x64.tar.xz
tar -Jxvf node-v$1-linux-x64.tar.xz
mv node-v$1-linux-x64 nodejs

# 添加ruby的path路径
echo 'export PATH=$PATH:$HOME/src/nodejs/bin' >> ~/.bashrc
