#!/bin/bash 

# 添加nginx, hhvm, mariadb软件源
nginx_list="/etc/apt/sources.list.d/nginx.list"
hhvm_list="/etc/apt/sources.list.d/hhvm.list"
mariadb_list="/etc/apt/sources.list.d/mariadb.list"

if [ ! -f "$nginx_list" ]; then
  wget -O - http://nginx.org/keys/nginx_signing.key | sudo apt-key add -
  echo 'deb http://nginx.org/packages/debian/ wheezy nginx' | sudo tee /etc/apt/sources.list.d/nginx.list
  echo 'deb-src http://nginx.org/packages/debian/ wheezy nginx' | sudo tee -a "$nginx_list"
fi

if [ ! -f "$hhvm_list" ]; then
  echo '140.211.166.134 dl.hhvm.com' | sudo tee -a /etc/hosts
  wget -O - http://dl.hhvm.com/conf/hhvm.gpg.key | sudo apt-key add -
  echo 'deb http://dl.hhvm.com/debian wheezy main' | sudo tee "$hhvm_list"
fi

if [ ! -f "$mariadb_list" ]; then
  sudo apt-key adv --recv-keys --keyserver keyserver.ubuntu.com 0xcbcb082a1bb943db
  echo 'deb http://mirrors.hustunique.com/mariadb/repo/5.5/debian wheezy main' | sudo tee "$mariadb_list"
fi

# 更新源
sudo apt-get update

# 安装nginx, hhvm
sudo apt-get install nginx -y
sudo apt-get install hhvm -y
sudo apt-get install libmemcachedutil2 -y

# 安装php相关
# wget -O - http://www.dotdeb.org/dotdeb.gpg | sudo apt-key add -
# deb http://packages.dotdeb.org wheezy-php55 all
# deb-src http://packages.dotdeb.org wheezy-php55 all

# 安装系统管理
sudo apt-get install sysv-rc-conf -y
sudo sysv-rc-conf nginx on
sudo sysv-rc-conf hhvm on

sudo service nginx restart
sudo service hhvm restart

exit