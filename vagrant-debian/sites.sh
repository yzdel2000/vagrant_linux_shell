#!/bin/bash 

block="server {
    listen 80;
    server_name localhost;
    root /vagrant/public;

    index index.html index.htm index.php;

    charset utf-8;

    location / {
        try_files \$uri \$uri/ /index.php?\$query_string;
    }

    location = /favicon.ico { access_log off; log_not_found off; }
    location = /robots.txt  { access_log off; log_not_found off; }

    access_log off;
    error_log  /var/log/nginx/error.log error;

    error_page 404 /index.php;

    include hhvm.conf;

    location ~ /\.ht {
        deny all;
    }
}
"

echo "$block" | sudo tee "/etc/nginx/conf.d/default.conf"
sudo service nginx restart
sudo service hhvm restart