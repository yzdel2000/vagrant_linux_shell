#!/bin/bash 

# 添加nginx, postgresql软件源
nginx_list="/etc/apt/sources.list.d/nginx.list"
pqsql_list="/etc/apt/sources.list.d/pqsql.list"

if [ ! -f "$nginx_list" ]; then
  wget -O - http://nginx.org/keys/nginx_signing.key | sudo apt-key add -
  echo 'deb http://nginx.org/packages/debian/ wheezy nginx' | sudo tee /etc/apt/sources.list.d/nginx.list
  echo 'deb-src http://nginx.org/packages/debian/ wheezy nginx' | sudo tee -a "$nginx_list"
fi

if [ ! -f "$pqsql_list" ]; then
  wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
  echo 'deb http://apt.postgresql.org/pub/repos/apt/ wheezy-pgdg main' | sudo tee "$pqsql_list"
fi

# 更新源
sudo apt-get update

# 安装nginx
sudo apt-get install nginx -y
sudo apt-get install postgresql-9.3

# 安装系统管理
sudo apt-get install sysv-rc-conf -y
sudo sysv-rc-conf nginx on

sudo service nginx restart

exit