var path = require('path');

module.exports = {
    entry: {
      app: './app/app.js'
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        publicPath: 'js/',
        filename: 'app.js'
    },
    resolve: {
        extensions: ['', '.js', 'css', 'less']
    },
    devServer: { 
        proxy: {
            "/api": "http://localhost:8000",
            "/api/*": "http://localhost:8000"
        }
    },
    module: {
        loaders: [
            {test: /\.js$/, exclude: /node_modules/, loader: "babel"},
            {test: /\.less$/, loader: 'style!css!less'},
            {test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "url?limit=10000&minetype=application/font-woff"},
            {test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file"}
        ]
    }
}