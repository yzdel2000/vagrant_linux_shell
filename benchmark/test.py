def fibonacci(n):
    if n < 2:
        return n
    return fibonacci(n - 2) + fibonacci(n - 1)

print(fibonacci(40))
# def factorial_tail(n, acc1, acc2):
#     if n < 2:
#         return acc1
#         # printf("factorial_tail(%d, %d, %d)\n", $n - 1, $acc2, $acc1 + $acc2);
#     return factorial_tail(n - 1, acc2, acc1 + acc2);
#
# print(factorial_tail(40, 1, 1));
