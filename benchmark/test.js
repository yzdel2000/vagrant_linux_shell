function fibonacci(n) {
  if (n < 2) {
    return n;
  }
  return fibonacci(n - 2) + fibonacci(n - 1);
}
console.log(fibonacci(40));

// function factorial_tail(n, acc1, acc2)
// {
//   if (n < 2)
//   {
//     return acc1;
//   }
//   else
//   {
//     return factorial_tail(n - 1, acc2, acc1 + acc2);
//   }
// }
// console.log(factorial_tail(40, 1, 1));
