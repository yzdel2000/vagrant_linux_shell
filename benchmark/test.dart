import 'dart:io';

int fibonacci(n) {
  if (n < 2) {
    return n;
  }
  return fibonacci(n - 2) + fibonacci(n - 1);
}

void main() {
  print(fibonacci(40));
}