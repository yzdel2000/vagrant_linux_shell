def fibonacci(n)
  if n < 2
    return n
  end
  return fibonacci(n - 2) + fibonacci(n - 1)
end
puts fibonacci(40)

# def factorial_tail(n, acc1, acc2)
#   if (n < 2)
#     return acc1
#   else
#     # printf("factorial_tail(%d, %d, %d)\n", $n - 1, $acc2, $acc1 + $acc2);
#     return factorial_tail(n - 1, acc2, acc1 + acc2);
#   end
# end
# puts factorial_tail(40, 1, 1);
