﻿using System;
using System.Diagnostics;

namespace Benchmark
{
    class Program
    {
        static void Main(string[] args)
        {
            var st = new Stopwatch();
            st.Start();
            var result = fibonacci(40);
            st.Stop();
            Console.WriteLine($"result: {result} {st.Elapsed}");
        }

        static int fibonacci(int n)
        {
            if (n < 2)
            {
                return n;
            }
            return fibonacci(n - 2) + fibonacci(n - 1);
        }
    }
}
