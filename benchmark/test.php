<?php
function fibonacci(int $n): int {
  if ($n < 2) {
    return $n;
  }
  return fibonacci($n - 2) + fibonacci($n - 1);
}
echo fibonacci(40)."\n";
// function factorial_tail($n, $acc1, $acc2)
// {
//   if ($n < 2)
//   {
//     return $acc1;
//   }
//   else
//   {
//     printf("factorial_tail(%d, %d, %d)\n", $n - 1, $acc2, $acc1 + $acc2);
//     return factorial_tail($n - 1, $acc2, $acc1 + $acc2);
//   }
// }
// echo factorial_tail(40, 1, 1)."\n";
