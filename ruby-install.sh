#!/bin/bash

# ruby-install 安装ruby
wget -O ruby-install-0.6.0.tar.gz https://github.com/postmodern/ruby-install/archive/v0.6.0.tar.gz
tar -xzvf ruby-install-0.6.0.tar.gz
cd ruby-install-0.6.0
sudo make install

# 默认安装最新稳定版
ruby-install -M https://ruby.taobao.org/mirrors/ruby ruby 2.3.1

# 添加ruby的path路径, root下会安装到opt目录
echo 'export PATH=$PATH:$HOME/.rubies/ruby-2.3.1/bin' >> ~/.bashrc

# gem加入淘宝源
export PATH=$PATH:$HOME/.rubies/ruby-2.3.1/bin
gem sources -a https://ruby.taobao.org/
gem sources -r https://rubygems.org/