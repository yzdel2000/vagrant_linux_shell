#!/bin/bash 

# debian最小默认安装没有curl
sudo apt-get install curl -y

# 以下的命令都是用户权限下操作的，任何命令最好都不要用sudo.
curl -L get.rvm.io | bash -s stable
source ~/.bashrc
source ~/.bash_profile

# 修改下载为ruby.taobao.org源
sed -i 's!cache.ruby-lang.org/pub/ruby!ruby.taobao.org/mirrors/ruby!' ~/.rvm/config/db

# 安装编译ruby所需要的包
# rvm requirements

# 安装MRI指定版本
rvm install 2.1.6
rvm use 2.1.6 --default

# gem修改为taobao源
gem sources -a https://ruby.taobao.org/
gem sources -r https://rubygems.org/

# 添加nodejs源 debian7必须/etc/apt/sources.list.d/chris-lea-node_js-wheezy.list修改wheey为lucid
apt-get install -y python-software-properties python g++ make
add-apt-repository ppa:chris-lea/node.js

exit