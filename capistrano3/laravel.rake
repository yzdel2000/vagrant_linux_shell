namespace :laravel do
  desc 'Upload .env'
  task :upload do
    on roles(:app) do |host|
      if test "[ ! -d #{shared_path}/ ]"
        execute "mkdir -p #{shared_path}/"
      end
      upload! ".env", "#{shared_path}/.env"
    end
  end
  
  desc 'Set dir permission'
  task :chmod_dir do
    on roles(:web) do
      within release_path do
        execute :chmod, '-R 777 public'
        execute :chmod, '-R 777 storage'
      end
    end
  end

  desc "Execute artisan command"
  task :artisan, :command_name do |t, args|
    # ask only runs if argument is not provided
    ask(:cmd, "list")
    command = args[:command_name] || fetch(:cmd)

    on roles(:web) do
      within release_path do
        execute :php, :artisan, command, *args.extras
      end
    end
  end

  desc "Execute composer command"
  task :composer, :command_name do |t, args|
    ask(:cmd, "list")
    command = args[:command_name] || fetch(:cmd)

    on roles(:web) do
      within release_path do
        execute :composer, command, *args.extras
      end
    end
  end
end